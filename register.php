<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        #main {
            width: 500px;
            border: 2px solid #4d7aa2;
            border-radius: 5px;
            padding: 30px 80px;
            display: block;
            margin: auto;
        }
        #main .box{

            width: 100%;
            display: flex;
            align-items: center;
            flex-direction: column;
        }
        #main .box .form-group{
            width: 100%;
            display: flex;
            margin-bottom: 15px;
        }
        form {
            width: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            margin-top: 15px;
        }
        .form-group {
            width: 100%;
            display: flex;
            margin-bottom: 15px;

        }
        .form-label{
            width: 20%;
            background-color: #5b9bd5;
            padding: 12px;
            border: 1px solid #4d7aa2;
            border-radius: 2px;
            margin-right: 15px;
            text-align: center;
            color: #fff;
        }

        form .form-button {
            background-color: rgb(101, 212, 110);
            padding: 12px 32px;
            font-size: 15px;
            border: 2px solid #4d7aa2;
            border-radius: 6px;
            color: #fff;
            max-width: 40%;
            text-align: center;
            margin-top: 32px;
        }
        
        .input_radio{
            margin-top: 10px;
            margin-left: 20px;
            font-size: large;
        }
        .form-input{
            width: 70%;
        }
        .select-options{
            width: 40%;
        }
    </style>
</head>
<body>
    <div id="main">
        <div class="box">
        <form action="" method="post">
                <?php echo '<div class="form-group">
                    <label class="form-label">Họ và tên</label>
                    <input class="form-input" type="text" name="username">
                    
                </div>' ?>
                <?php 
                $gender=array(0,1);

                echo '<div class="form-group">
                        <label class="form-label">Giới tính</label>
                    ';

                for ($i = 0; $i < count($gender); $i++) {
                    if ($gender[$i] == 0){
                        echo '
                        <div class="input_radio"> 
                            <input  type="radio" id="0" name="sex" value="">
                            <label  for="0">Nam</label> 
                        </div>
                    '; 
                    }
                    else{
                    echo '
                        <div class="input_radio">
                            <input type="radio" id="1" name="sex" value="">
                            <label  for="1">Nữ</label>
                        </div>
                    </div>';
                    }
                } 
                ?>

                <?php 
                $khoa=array('MAT','KDL');
                echo '<div class="form-group">
                        <label class="form-label">Phân Khoa</label>
                    
                    <select class="select-options" name="khoa" id="khoa">
                    <option value=""></option>
                    ' 
                    ;
                foreach($khoa as $i){
                    if ($i == "MAT"){
                        echo '<div class="form-group">
                            <option value="MAT">Khoa học máy tính</option>
                        </div>';
                    }
                    if ($i == "KDL"){
                        echo '<div class="form-group">
                            <option value="KDL">Khoa học vật liệu</option> 
                        </div>';}
                };
                echo '</select>
                    </div>';
                ?>

                <button class="form-button">Đăng ký</button>
            </form>

        </div>

    </div>
    
</body>
</html>